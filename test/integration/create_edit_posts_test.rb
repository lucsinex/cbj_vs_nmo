require 'test_helper'

class CreateEditPostsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    @blogpost = blogposts(:one)
    get admin_path, nil, 'HTTP_AUTHORIZATION' => ActionController::HttpAuthentication::Basic.encode_credentials('admin', 'password')
  end
  
  test "should delete posts" do
    assert_difference 'Blogpost.count', -1 do
      delete blogpost_path(@blogpost)
    end 
  end
end
