require 'test_helper'

class BlogpostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @blogpost = Blogpost.new
  end
  
  test "should not save without title" do
    @blogpost.title = ""
    assert_not @blogpost.save
  end
  
  test "should not save without content" do
    @blogpost.content = ""
    assert_not @blogpost.save
  end
end
