class StaticPagesController < ApplicationController
  def about
  end

  def contact
  end
  
  def home
  end
  
  def submit
  end
  
  def info
  end
end
