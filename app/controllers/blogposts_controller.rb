class BlogpostsController < ApplicationController
  http_basic_authenticate_with :name => "admin", :password => "password", :except => ['index']
  
  def show
    @blogpost = Blogpost.find(params[:id])
  end
  
  def new
    @blogpost = Blogpost.new
  end
  
  def edit
    @blogpost = Blogpost.find(params[:id])
  end
  
  def update
    @blogpost = Blogpost.find(params[:id])
    if @blogpost.update_attributes(post_params)
      flash.notice = "Post successfully updated"
      redirect_to admin_path
    else
      render 'edit'
    end
  end
  
  def create
    @blogpost = Blogpost.new(post_params)
    if @blogpost.save
      flash.notice = "Post successfully created"
      redirect_to admin_path
    else
      render 'new'
    end
  end
  
  def index
    @blogposts = Blogpost.all
  end
  
  def admin
    @blogposts = Blogpost.all
  end
  
  def destroy
    @blogpost = Blogpost.find(params[:id])
    @blogpost.destroy
    redirect_to admin_path
  end
  
  private

    def post_params
      params.require(:blogpost).permit(:title, :content)
    end
end
